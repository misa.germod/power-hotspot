package com.germod.powerhostspot

import android.content.Intent
import android.content.BroadcastReceiver
import android.content.Context
import android.media.AudioManager
import android.media.ToneGenerator
import android.net.wifi.WifiManager
import androidx.appcompat.app.AppCompatActivity
import java.lang.reflect.Method

class PowerConnectionReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        ToneGenerator(AudioManager.STREAM_MUSIC, 100).
        startTone(ToneGenerator.TONE_CDMA_PIP, 150)

        val isCharging = intent.action == Intent.ACTION_POWER_CONNECTED
        setWifiTetheringEnabled(context, isCharging)
    }

    private fun setWifiTetheringEnabled(context: Context, enable: Boolean) {
        val wifiManager = context.applicationContext.getSystemService(AppCompatActivity.WIFI_SERVICE) as WifiManager
        val methods: Array<Method> = wifiManager.javaClass.declaredMethods
        for (method in methods) {
            if (method.name.equals("setWifiApEnabled")) {
                try {
                    method.invoke(wifiManager, null, enable)
                } catch (ex: Exception) {
                    print(ex)
                }
                break
            }
        }
    }
}